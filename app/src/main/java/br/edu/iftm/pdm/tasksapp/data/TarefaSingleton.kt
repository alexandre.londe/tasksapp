package br.edu.iftm.pdm.tasksapp.data

import androidx.compose.runtime.mutableStateListOf
import br.edu.iftm.pdm.tasksapp.model.Tarefa

object TarefaSingleton {
    private val tarefas = mutableStateListOf<Tarefa>()
    fun updateTarefaList(tarefas: ArrayList<Tarefa>) {
        this.tarefas.clear()
        this.tarefas.addAll(tarefas)
    }
    fun getTarefas(): List<Tarefa> {
        return this.tarefas
    }
}