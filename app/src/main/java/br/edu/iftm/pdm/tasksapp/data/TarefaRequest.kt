package br.edu.iftm.pdm.tasksapp.data

import android.content.Context
import android.os.Looper
import com.android.volley.toolbox.Volley
import android.os.Handler
import android.util.Log
import br.edu.iftm.pdm.tasksapp.model.Tarefa
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONArray
import org.json.JSONObject

class TarefaRequest(context: Context) {
    private val queue = Volley.newRequestQueue(context)

    companion object {
        private val API_URL = "http://10.0.2.2:8000"
        private val GET_TAREFAS = "/tasks"
        private val POST_TAREFAS = "/tasks/new"
    }

    fun startTarefaRequest() {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                tarefasRequest()
                handler.postDelayed(this,5000)
            }
        })
    }

    fun JSONArrayToTarefaList(jsonArray: JSONArray): ArrayList<Tarefa> {
        val tarefas = ArrayList<Tarefa>()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObject = jsonArray.getJSONObject(i)
            val task = Tarefa(
                jsonObject.getInt("id"),
                jsonObject.getString("description"),
                jsonObject.getBoolean("is_done"),
                jsonObject.getBoolean("is_urgent"),
            )
            tarefas.add(task)
        }
        return tarefas
    }

    private fun tarefasRequest() {
        var jsonRequest = JsonArrayRequest(
            Request.Method.GET,
            API_URL + GET_TAREFAS,
            null,
            { response ->
                val taskList = JSONArrayToTarefaList(response)
                TarefaSingleton.updateTarefaList(taskList)
            },
            { error ->
                Log.e("TAREFAREQERR", "Tarefa request error: ${error}")
            }
        )
        this.queue.add(jsonRequest)
    }

    fun addTarefa(tarefa: Tarefa) {
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.POST,
            API_URL + POST_TAREFAS,
            this.tarefaToJSON(tarefa),
            { response ->
                Log.d("RequestResponse", response.toString())
                this.tarefasRequest()
            },
            { volleyError ->
                Log.e("RequestTarefaError", "Connection error. ${volleyError.toString()}")
            }
        )

        this.queue.add(jsonArrayRequest);
    }

    fun tarefaToJSON(task: Tarefa): JSONObject {
        val jsonObject = JSONObject();
        jsonObject.put("description",task.description);
        jsonObject.put("is_done",task.is_done);
        jsonObject.put("is_urgent",task.is_urgent);

        return jsonObject;
    }
}