package br.edu.iftm.pdm.tasksapp

import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import br.edu.iftm.pdm.tasksapp.data.TarefaRequest
import br.edu.iftm.pdm.tasksapp.ui.screens.MainScreenView
import br.edu.iftm.pdm.tasksapp.ui.theme.TasksAppTheme

class MainActivity : ComponentActivity() {
    private lateinit var tarefasRequest: TarefaRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.tarefasRequest = TarefaRequest(this)
        this.tarefasRequest.startTarefaRequest()
        setContent {
            TasksAppTheme {
                this.SetupUIConfigs()
                MainScreenView(this.tarefasRequest)
            }
        }
    }

    @Composable
    private fun SetupUIConfigs() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (MaterialTheme.colors.isLight) {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            } else {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        0, WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            }
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }
}