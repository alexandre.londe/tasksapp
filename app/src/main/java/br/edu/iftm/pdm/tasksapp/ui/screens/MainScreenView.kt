package br.edu.iftm.pdm.tasksapp.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.edu.iftm.pdm.tasksapp.data.TarefaRequest
import br.edu.iftm.pdm.tasksapp.R
import br.edu.iftm.pdm.tasksapp.data.TarefaSingleton
import br.edu.iftm.pdm.tasksapp.model.Tarefa
import br.edu.iftm.pdm.tasksapp.ui.components.TarefaItemView


@Composable
fun MainScreenView(tarefaRequest: TarefaRequest) {
    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.secondary,
                modifier = Modifier.padding(bottom = 10.dp)
            ) {
                Text(
                    text = stringResource(id = R.string.app_name),
                    fontSize = 20.sp,
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Left,
                    color = MaterialTheme.colors.onSecondary
                )
            }
        },
        bottomBar = {
            footer(tarefaRequest)
        }
    ) { innerPadding ->
        listaTarefas(innerPadding = innerPadding)
    }
}

@Composable
fun listaTarefas(innerPadding: PaddingValues) {

    Box(modifier = Modifier.padding(innerPadding)){
        Column() {
            LazyColumn() {
                items(TarefaSingleton.getTarefas()){ tarefa ->
                    TarefaItemView(tarefa = tarefa)
                }
            }
        }
    }
}

@Composable
fun footer(tarefaRequest: TarefaRequest) {
    val urgente = remember { mutableStateOf(false) }
    val descricaoTarefa = remember { mutableStateOf(TextFieldValue("")) }
    Column {
        Row {
            Text(
                text = stringResource(id = R.string.urgente),
                textAlign = TextAlign.Left,
                modifier = Modifier.padding(start = 10.dp, end = 10.dp),
                fontSize = 20.sp,
                color = MaterialTheme.colors.onSecondary
            )
            Switch(
                checked = urgente.value,
                onCheckedChange = {
                    urgente.value = !urgente.value
                },
                colors = SwitchDefaults.colors(
                    uncheckedThumbColor = MaterialTheme.colors.secondary,
                    checkedThumbColor = MaterialTheme.colors.onSecondary
                )
            )
        }
        Row {
            OutlinedTextField(
                value = descricaoTarefa.value,
                onValueChange = { value ->
                    descricaoTarefa.value = value
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(textColor = MaterialTheme.colors.onSecondary),
                placeholder = { Text(stringResource(id = R.string.message_criar)) },
                modifier = Modifier
                    .padding(start = 10.dp, end = 10.dp, top = 5.dp, bottom = 5.dp)
                    .background(MaterialTheme.colors.primary)
                    .fillMaxWidth()
            )
        }
        Row {
            Button(
                onClick = {
                    tarefaRequest.addTarefa(Tarefa(0, description = descricaoTarefa.value.text, is_urgent = urgente.value, is_done = false))
                    descricaoTarefa.value = TextFieldValue("")
                    urgente.value = false
                },
                modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, end = 10.dp, bottom = 10.dp)
            ) {
                Text(stringResource(id = R.string.botao_ok), color = MaterialTheme.colors.onSecondary)
            }
        }
    }

}
