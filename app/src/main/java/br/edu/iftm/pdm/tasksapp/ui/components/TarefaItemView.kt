package br.edu.iftm.pdm.tasksapp.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import br.edu.iftm.pdm.tasksapp.R
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import br.edu.iftm.pdm.tasksapp.model.Tarefa
import br.edu.iftm.pdm.tasksapp.ui.theme.TasksAppTheme

@Composable
fun TarefaItemView(tarefa: Tarefa) {
    var concluido = remember { mutableStateOf(tarefa.is_done) }
    var tamanhoMax = remember { mutableStateOf(1) };


    Card(
        backgroundColor = MaterialTheme.colors.primary,
        modifier =
        Modifier
            .padding(horizontal = 5.dp, vertical = 2.5.dp)
            .fillMaxWidth()
    ){
        Row (
            modifier = Modifier
                .height(intrinsicSize = IntrinsicSize.Min)
                .clickable {
                    if (tamanhoMax.value == 1) tamanhoMax.value = 1000 else tamanhoMax.value = 1
                }
                ){
            Box(modifier =
            Modifier
                .background(if (tarefa.is_urgent) Color.Red else Color.Green)
                .width(9.dp)
                .fillMaxHeight()
            );
            Text(
                tarefa.description,
                overflow = TextOverflow.Ellipsis,
                maxLines = tamanhoMax.value,
                modifier = Modifier
                    .padding(start = 5.dp, end = 10.dp)
                    .fillMaxWidth()
                    .weight(1f)
                    .align(alignment = Alignment.CenterVertically)
            )
            Text(
                stringResource(id = R.string.feito),
                modifier =
                Modifier
                    .align(Alignment.CenterVertically),
            )
            Checkbox(
                checked = concluido.value,
                onCheckedChange = {
                    concluido.value = !concluido.value
                    tarefa.is_done = concluido.value
                },
                colors = CheckboxDefaults.colors(
                    checkedColor = MaterialTheme.colors.onSecondary,
                ),
                modifier = Modifier
                    .width(30.dp)
                    .align(alignment = Alignment.CenterVertically)
            );
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewTaskItemView() {
    val task = Tarefa(0, "CMAOSMCIASIFUASJOIDMNJFHBADIOASMDNSIBFAUSDIASODMFASNHDVAUSDISAFA", false, true)
    TasksAppTheme(darkTheme = false) {
        TarefaItemView(task)
    }
}

