package br.edu.iftm.pdm.tasksapp.model

class Tarefa (
    val id: Int,
    val description: String,
    var is_done: Boolean = false,
    val is_urgent: Boolean,
){
}